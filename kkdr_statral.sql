-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Feb 2021 pada 14.46
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kkdr_statral`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(20) NOT NULL,
  `kategori` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`) VALUES
(1, 'Geografis'),
(2, 'Pemerintahan'),
(3, 'Kependudukan'),
(4, 'Pendidikan'),
(5, 'Kesehatan'),
(6, 'Agama dan Sosial Lainnya'),
(7, 'Perdagangan, Perindustrian, Pariwisata & Pertanian'),
(8, 'Transportasi dan Komunikasi'),
(9, 'Keuangan, PDRB, IPM, dan Perbandingan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(20) NOT NULL,
  `kecamatan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `kecamatan`) VALUES
(1, 'Mojoroto'),
(2, 'Kota'),
(3, 'Pesantren');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id_kelurahan` int(20) NOT NULL,
  `kelurahan` varchar(200) NOT NULL,
  `id_kecamatan` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelurahan`
--

INSERT INTO `kelurahan` (`id_kelurahan`, `kelurahan`, `id_kecamatan`) VALUES
(1, 'Manisrenggo', 2),
(2, 'Rejomulyo', 2),
(3, 'Ngronggo', 2),
(4, 'Kaliombo', 2),
(5, 'Kampungdalem', 2),
(6, 'Setonopande', 2),
(7, 'Ringinanom', 2),
(8, 'Pakelan', 2),
(9, 'Setonogedong', 2),
(10, 'Kemasan', 2),
(11, 'Jagalan', 2),
(12, 'Banjaran', 2),
(13, 'Ngadirejo', 2),
(14, 'Dandangan', 2),
(15, 'Balowerti', 2),
(16, 'Pocanan', 2),
(17, 'Semampir', 2),
(18, 'Pojok', 1),
(19, 'Campurejo', 1),
(20, 'Tamanan', 1),
(21, 'Banjarmlati', 1),
(22, 'Bandar Kidul', 1),
(23, 'Lirboyo', 1),
(24, 'Bandar Lor', 1),
(25, 'Mojoroto', 1),
(26, 'Sukorame', 1),
(27, 'Bujel', 1),
(28, 'Ngampel', 1),
(29, 'Gayam', 1),
(30, 'Mrican', 1),
(31, 'Dermo', 1),
(32, 'Blabak', 3),
(33, 'Bawang', 3),
(34, 'Betet', 3),
(35, 'Tosaren', 3),
(36, 'Banaran', 3),
(37, 'Ngletih', 3),
(38, 'Tempurejo', 3),
(39, 'Ketami', 3),
(40, 'Pesantren', 3),
(41, 'Bangsal', 3),
(42, 'Burengan', 3),
(43, 'Tinalan', 3),
(44, 'Pakunden', 3),
(45, 'Singonegaran', 3),
(46, 'Jamsaren', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan`
--

CREATE TABLE `luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan` (
  `id_luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan` int(200) NOT NULL,
  `id_kelurahan` int(200) NOT NULL,
  `luas_km_2` float NOT NULL,
  `kode_sub_kategori` varchar(200) NOT NULL,
  `id_kecamatan` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan`
--

INSERT INTO `luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan` (`id_luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan`, `id_kelurahan`, `luas_km_2`, `kode_sub_kategori`, `id_kecamatan`) VALUES
(1, 18, 5.153, '1.1.2', 1),
(2, 19, 1.409, '1.1.2', 1),
(3, 20, 1.077, '1.1.2', 1),
(4, 21, 0.954, '1.1.2', 1),
(5, 22, 1.299, '1.1.2', 1),
(6, 23, 1.037, '1.1.2', 1),
(7, 24, 1.113, '1.1.2', 1),
(8, 25, 2.13, '1.1.2', 1),
(9, 26, 4.302, '1.1.2', 1),
(10, 27, 1.59, '1.1.2', 1),
(11, 28, 1.468, '1.1.2', 1),
(12, 29, 1.296, '1.1.2', 1),
(13, 30, 1.109, '1.1.2', 1),
(14, 31, 0.657, '1.1.2', 1),
(15, 1, 1.764, '1.1.3', 2),
(16, 2, 1.67, '1.1.3', 2),
(17, 3, 2.585, '1.1.3', 2),
(18, 4, 0.958, '1.1.3', 2),
(19, 5, 0.332, '1.1.3', 2),
(20, 6, 0.383, '1.1.3', 2),
(21, 7, 0.05, '1.1.3', 2),
(22, 8, 0.214, '1.1.3', 2),
(23, 9, 0.059, '1.1.3', 2),
(24, 10, 0.228, '1.1.3', 2),
(25, 11, 0.043, '1.1.3', 2),
(26, 12, 1.209, '1.1.3', 2),
(27, 13, 1.47, '1.1.3', 2),
(28, 14, 1.1, '1.1.3', 2),
(29, 15, 0.83, '1.1.3', 2),
(30, 16, 0.214, '1.1.3', 2),
(31, 17, 1.791, '1.1.3', 2),
(32, 32, 3.354, '1.1.4', 3),
(33, 33, 3.449, '1.1.4', 3),
(34, 34, 1.691, '1.1.4', 3),
(35, 35, 1.361, '1.1.4', 3),
(36, 36, 0.974, '1.1.4', 3),
(37, 37, 1.237, '1.1.4', 3),
(38, 38, 1.864, '1.1.4', 3),
(39, 39, 1.894, '1.1.4', 3),
(40, 40, 1.356, '1.1.4', 3),
(41, 41, 1.029, '1.1.4', 3),
(42, 42, 1.283, '1.1.4', 3),
(43, 43, 0.926, '1.1.4', 3),
(44, 44, 1.024, '1.1.4', 3),
(45, 45, 0.99, '1.1.4', 3),
(46, 46, 1.471, '1.1.4', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `luas_wilayah_per_kecamatan`
--

CREATE TABLE `luas_wilayah_per_kecamatan` (
  `id_luas_wilayah_kota_kediri_per_kecamatan` int(20) NOT NULL,
  `id_kecamatan` int(20) NOT NULL,
  `luas_km_2` float NOT NULL,
  `kode_sub_kategori` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `luas_wilayah_per_kecamatan`
--

INSERT INTO `luas_wilayah_per_kecamatan` (`id_luas_wilayah_kota_kediri_per_kecamatan`, `id_kecamatan`, `luas_km_2`, `kode_sub_kategori`) VALUES
(1, 1, 24.6, '1.1.1'),
(2, 2, 14.9, '1.1.1'),
(3, 3, 23.9, '1.1.1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_kategori`
--

CREATE TABLE `sub_kategori` (
  `id_sub_kategori` int(20) NOT NULL,
  `sub_kategori` text NOT NULL,
  `id_kategori` int(20) NOT NULL,
  `kode` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_kategori`
--

INSERT INTO `sub_kategori` (`id_sub_kategori`, `sub_kategori`, `id_kategori`, `kode`) VALUES
(1, 'Luas Wilayah Kota Kediri per Kecamatan', 1, '1.1.1'),
(2, 'Luas Wilayah Kecamatan Mojoroto per Kelurahan', 1, '1.1.2'),
(3, 'Luas Wilayah Kecamatan Kota per Kelurahan', 1, '1.1.3'),
(4, 'Luas Wilayah Kecamatan Pesantren per Kelurahan', 1, '1.1.4'),
(5, 'Jarak Kantor Kelurahan ke Kantor Kecamatan Mojoroto dan ke Kantor Balai ', 1, '1.2.1'),
(6, 'Jarak Kantor Kelurahan ke Kantor Kecamatan Kota dan ke Kantor Balai Kota', 1, '1.2.2'),
(7, 'Jarak Kantor Kelurahan ke Kantor Kecamatan Pesantren dan ke Kantor Balai ', 1, '1.2.3'),
(8, 'Rata-Rata Ketinggian Tempat dari Permukaan Air Laut dan Kedalaman Sumur di Kecamatan Mojoroto', 1, '1.3.1'),
(9, 'Rata-Rata Ketinggian Tempat dari Permukaan Air Laut dan Kedalaman Sumur ', 1, '1.3.2'),
(10, 'Rata-Rata Ketinggian Tempat dari Permukaan Air Laut dan Kedalaman Sumur di Kecamatan Pesantren', 1, '1.3.3'),
(11, 'Nama dan Panjang Sungai di Kota Kediri 2015', 1, '1.4.1');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indeks untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`);

--
-- Indeks untuk tabel `luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan`
--
ALTER TABLE `luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan`
  ADD PRIMARY KEY (`id_luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan`);

--
-- Indeks untuk tabel `luas_wilayah_per_kecamatan`
--
ALTER TABLE `luas_wilayah_per_kecamatan`
  ADD PRIMARY KEY (`id_luas_wilayah_kota_kediri_per_kecamatan`);

--
-- Indeks untuk tabel `sub_kategori`
--
ALTER TABLE `sub_kategori`
  ADD PRIMARY KEY (`id_sub_kategori`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_kecamatan` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id_kelurahan` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT untuk tabel `luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan`
--
ALTER TABLE `luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan`
  MODIFY `id_luas_wilayah_kecamatan_mojoroto_kota_pesantren_per_kelurahan` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT untuk tabel `luas_wilayah_per_kecamatan`
--
ALTER TABLE `luas_wilayah_per_kecamatan`
  MODIFY `id_luas_wilayah_kota_kediri_per_kecamatan` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `sub_kategori`
--
ALTER TABLE `sub_kategori`
  MODIFY `id_sub_kategori` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
