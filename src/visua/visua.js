import React, { useState } from 'react'
import {Bar, Doughnut, HorizontalBar, Line, Pie,Scatter} from 'react-chartjs-2'
//import 'chartjs-plugin-datalabels';
import {Fuck} from '../allseeing'
import {add_to_datatable_filter,clear_datatable_filter} from '../rdx/action'
import { saveAs } from 'file-saver';
var _ = require('lodash');
var moment = require('moment')

var randomColor = require('randomcolor');

class Visua extends React.PureComponent{
    constructor(){
        super();
        this.state = {
            datatable:[],
            kolom:[],
            kode:'',
            show:false,
            datane:[],
            thn_start:[],
            thn_end:[],
            start:'',
            end:'',
        }

    }

    componentDidMount(){
        const datatable= this.props.prod.datatable_filter
        this.setState({kolom:this.props.kolom,kode:this.props.kode})
        this.get_year()
        this.which(datatable)
    }

    which(datatable){
        if(this.props.kode == "1.1.1"){
            this.get_visua_1_1_1(datatable)
        }else if(this.props.kode == "1.1.2" ){
            this.get_visua_1_1_2__1_1_3__1_1_4(datatable)
        }else if(this.props.kode == "1.1.3" ){
            this.get_visua_1_1_2__1_1_3__1_1_4(datatable)
        }else if(this.props.kode == "1.1.4"){
            this.get_visua_1_1_2__1_1_3__1_1_4(datatable)
        }else if(this.props.kode == "1.2.1"){
            this.get_visua_1_2_1__1_2_2__1_2_3(datatable)
        }else if(this.props.kode == "1.2.2"){
            this.get_visua_1_2_1__1_2_2__1_2_3(datatable)
        }else if(this.props.kode == "1.2.3"){
            this.get_visua_1_2_1__1_2_2__1_2_3(datatable)
        }else if(this.props.kode == "1.3.1"){
            this.get_visua_1_3_1__1_3_2__1_3_3(datatable)
        }else if(this.props.kode == "1.3.2"){
            this.get_visua_1_3_1__1_3_2__1_3_3(datatable)
        }else if(this.props.kode == "1.3.3"){
            this.get_visua_1_3_1__1_3_2__1_3_3(datatable)
        }else if(this.props.kode == "1.4.1"){
            this.get_visua_1_4_1(datatable)
        }else if(this.props.kode == "2.1.1"){
            this.get_visua_2_1_1(datatable)
        }else if(this.props.kode == "2.1.2"){
            this.get_visua_2_1_2__2_1_3__2_1_4(datatable)
        }else if(this.props.kode == "2.1.3"){
            this.get_visua_2_1_2__2_1_3__2_1_4(datatable)
        }else if(this.props.kode == "2.1.4"){
            this.get_visua_2_1_2__2_1_3__2_1_4(datatable)
        }else if(this.props.kode == "2.2.1"){
            this.get_visua_2_2_1__2_2_2__2_2_3(datatable)
        }else if(this.props.kode == "2.2.2"){
            this.get_visua_2_2_1__2_2_2__2_2_3(datatable)
        }else if(this.props.kode == "2.2.3"){
            this.get_visua_2_2_1__2_2_2__2_2_3(datatable)
        }else if(this.props.kode == "3.1.1"){
            this.get_visua_3_1_1(datatable)
        }else if(this.props.kode == "3.2.1"){
            this.get_visua_3_2_1(datatable)
        }else if(this.props.kode == "3.2.2"){
            this.get_visua_3_2_2__3_2_3__3_2_4(datatable)
        }else if(this.props.kode == "3.2.3"){
            this.get_visua_3_2_2__3_2_3__3_2_4(datatable)
        }else if(this.props.kode == "3.2.4"){
            this.get_visua_3_2_2__3_2_3__3_2_4(datatable)
        }else if(this.props.kode == "3.3.1"){
            this.get_visua_3_3_1__3_3_2__3_3_3(datatable)
        }else if(this.props.kode == "3.3.2"){
            this.get_visua_3_3_1__3_3_2__3_3_3(datatable)
        }else if(this.props.kode == "3.3.3"){
            this.get_visua_3_3_1__3_3_2__3_3_3(datatable)
        }else if(this.props.kode == "3.4.1"){
            this.get_visua_3_4_1(datatable)
        }else if(this.props.kode == "3.5.1"){
            this.get_visua_3_5_1__3_5_2__3_5_3(datatable)
        }else if(this.props.kode == "3.5.2"){
            this.get_visua_3_5_1__3_5_2__3_5_3(datatable)
        }else if(this.props.kode == "3.5.3"){
            this.get_visua_3_5_1__3_5_2__3_5_3(datatable)
        }else if(this.props.kode == "3.6.1"){
            this.get_visua_3_6_1(datatable)
        }else if(this.props.kode == "3.7.1"){
            this.get_visua_3_7_1(datatable)
        }else if(this.props.kode == "3.7.2"){
            this.get_visua_3_7_2__3_7_3__3_7_4(datatable)
        }else if(this.props.kode == "3.7.3"){
            this.get_visua_3_7_2__3_7_3__3_7_4(datatable)
        }else if(this.props.kode == "3.7.4"){
            this.get_visua_3_7_2__3_7_3__3_7_4(datatable)
        }else if(this.props.kode == "4.1.1"){
            this.get_visua_4_1_1(datatable)
        }else if(this.props.kode == "4.2.1"){
            this.get_visua_4_2_1(datatable)
        }else if(this.props.kode == "4.3.1"){
            this.get_visua_4_3_1(datatable)
        }else if(this.props.kode == "4.4.1"){
            this.get_visua_4_4_1(datatable)
        }else if(this.props.kode == "4.5.1"){
            this.get_visua_4_5_1__4_5_2__4_5_3(datatable)
        }else if(this.props.kode == "4.5.2"){
            this.get_visua_4_5_1__4_5_2__4_5_3(datatable)
        }else if(this.props.kode == "4.5.3"){
            this.get_visua_4_5_1__4_5_2__4_5_3(datatable)
        }else if(this.props.kode == "4.6.1"){
            this.get_visua_4_6_1__4_6_2__4_6_3(datatable)
        }else if(this.props.kode == "4.6.2"){
            this.get_visua_4_6_1__4_6_2__4_6_3(datatable)
        }else if(this.props.kode == "4.6.3"){
            this.get_visua_4_6_1__4_6_2__4_6_3(datatable)
        }else if(this.props.kode == "4.7.1"){
            this.get_visua_4_7_1__4_7_2__4_7_3(datatable)
        }else if(this.props.kode == "4.7.2"){
            this.get_visua_4_7_1__4_7_2__4_7_3(datatable)
        }else if(this.props.kode == "4.7.3"){
            this.get_visua_4_7_1__4_7_2__4_7_3(datatable)
        }else if(this.props.kode == "4.8.1"){
            this.get_visua_4_8_1__4_8_2__4_8_3(datatable)
        }else if(this.props.kode == "4.8.2"){
            this.get_visua_4_8_1__4_8_2__4_8_3(datatable)
        }else if(this.props.kode == "4.8.3"){
            this.get_visua_4_8_1__4_8_2__4_8_3(datatable)
        }else if(this.props.kode == "4.10.1"){
            this.get_visua_4_10_1__4_10_2__4_10_3(datatable)
        }else if(this.props.kode == "4.10.2"){
            this.get_visua_4_10_1__4_10_2__4_10_3(datatable)
        }else if(this.props.kode == "4.10.3"){
            this.get_visua_4_10_1__4_10_2__4_10_3(datatable)
        }else if(this.props.kode == "4.11.1"){
            this.get_visua_4_11_1(datatable)
        }else if(this.props.kode == "4.11.2"){
            this.get_visua_4_11_2(datatable)
        }else if(this.props.kode == "4.11.3"){
            this.get_visua_4_11_3(datatable)
        }else if(this.props.kode == "4.11.4"){
            this.get_visua_4_11_4(datatable)
        }else if(this.props.kode == "4.11.5"){
            this.get_visua_4_11_5(datatable)
        }else if(this.props.kode == "5.1.1"){
            this.get_visua_5_1_1(datatable)
        }else if(this.props.kode == "5.1.2"){
            this.get_visua_5_1_2__5_1_3__5_1_4(datatable)
        }else if(this.props.kode == "5.1.3"){
            this.get_visua_5_1_2__5_1_3__5_1_4(datatable)
        }else if(this.props.kode == "5.1.4"){
            this.get_visua_5_1_2__5_1_3__5_1_4(datatable)
        }else if(this.props.kode == "5.2.1"){
            this.get_visua_5_2_1(datatable)
        }else if(this.props.kode == "5.3.1"){
            this.get_visua_5_3_1(datatable)
        }else if(this.props.kode == "5.4.1"){
            this.get_visua_5_4_1__5_4_2__5_4_3(datatable)
        }else if(this.props.kode == "5.4.2"){
            this.get_visua_5_4_1__5_4_2__5_4_3(datatable)
        }else if(this.props.kode == "5.4.3"){
            this.get_visua_5_4_1__5_4_2__5_4_3(datatable)
        }else if(this.props.kode == "5.5.1"){
            this.get_visua_5_5_1__5_5_2__5_5_3(datatable)
        }else if(this.props.kode == "5.5.2"){
            this.get_visua_5_5_1__5_5_2__5_5_3(datatable)
        }else if(this.props.kode == "5.5.3"){
            this.get_visua_5_5_1__5_5_2__5_5_3(datatable)
        }else if(this.props.kode == "5.6.1"){
            this.get_visua_5_6_1(datatable)
        }else if(this.props.kode == "5.6.2"){
            this.get_visua_5_6_2(datatable)
        }else if(this.props.kode == "5.7.1"){
            this.get_visua_5_7_1(datatable)
        }else if(this.props.kode == "5.7.2"){
            this.get_visua_5_7_2(datatable)
        }else if(this.props.kode == "5.7.3"){
            this.get_visua_5_7_3(datatable)
        }else if(this.props.kode == "5.8.1"){
            this.get_visua_5_8_1(datatable)
        }else if(this.props.kode == "5.8.2"){
            this.get_visua_5_8_2(datatable)
        }else if(this.props.kode == "5.9.1"){
            this.get_visua_5_9_1(datatable)
        }else if(this.props.kode == "6.1.1"){
            this.get_visua_6_1_1(datatable)
        }else if(this.props.kode == "6.2.1"){
            this.get_visua_6_2_1(datatable)
        }else if(this.props.kode == "6.2.2"){
            this.get_visua_6_2_2__6_2_3__6_2_4(datatable)
        }else if(this.props.kode == "6.2.3"){
            this.get_visua_6_2_2__6_2_3__6_2_4(datatable)
        }else if(this.props.kode == "6.2.4"){
            this.get_visua_6_2_2__6_2_3__6_2_4(datatable)
        }else if(this.props.kode == "6.3.1"){
            this.get_visua_6_3_1__6_3_2__6_3_3(datatable)
        }else if(this.props.kode == "6.3.2"){
            this.get_visua_6_3_1__6_3_2__6_3_3(datatable)
        }else if(this.props.kode == "6.3.3"){
            this.get_visua_6_3_1__6_3_2__6_3_3(datatable)
        }else if(this.props.kode == "6.4.1"){
            this.get_visua_6_4_1(datatable)
        }else if(this.props.kode == "6.4.2"){
            this.get_visua_6_4_2(datatable)
        }else if(this.props.kode == "6.4.3"){
            this.get_visua_6_4_3(datatable)
        }else if(this.props.kode == "6.4.4"){
            this.get_visua_6_4_4(datatable)
        }else if(this.props.kode == "6.5.1"){
            this.get_visua_6_5_1(datatable)
        }else if(this.props.kode == "7.1.1"){
            this.get_visua_7_1_1(datatable)
        }else if(this.props.kode == "7.1.2"){
            this.get_visua_7_1_2__7_1_3__7_1_4(datatable)
        }else if(this.props.kode == "7.1.3"){
            this.get_visua_7_1_2__7_1_3__7_1_4(datatable)
        }else if(this.props.kode == "7.1.4"){
            this.get_visua_7_1_2__7_1_3__7_1_4(datatable)
        }else if(this.props.kode == "7.2.1"){
            this.get_visua_7_2_1(datatable)
        }else if(this.props.kode == "7.2.2"){
            this.get_visua_7_2_2(datatable)
        }else if(this.props.kode == "7.3.1"){
            this.get_visua_7_3_1__7_3_2__7_3_3(datatable)
        }else if(this.props.kode == "7.3.2"){
            this.get_visua_7_3_1__7_3_2__7_3_3(datatable)
        }else if(this.props.kode == "7.3.3"){
            this.get_visua_7_3_1__7_3_2__7_3_3(datatable)
        }else if(this.props.kode == "7.4.1"){
            this.get_visua_7_4_1(datatable)
        }else if(this.props.kode == "7.5.1"){
            this.get_visua_7_5_1(datatable)
        }else if(this.props.kode == "7.6.1"){
            this.get_visua_7_6_1(datatable)
        }else if(this.props.kode == "7.6.2"){
            this.get_visua_7_6_2(datatable)
        }else if(this.props.kode == "7.7.1"){
            this.get_visua_7_7_1(datatable)
        }else if(this.props.kode == "7.7.2"){
            this.get_visua_7_7_2(datatable)
        }else if(this.props.kode == "7.8.1"){
            this.get_visua_7_8_1(datatable)
        }else if(this.props.kode == "7.9.1"){
            this.get_visua_7_9_1(datatable)
        }else if(this.props.kode == "7.10.1"){
            this.get_visua_7_10_1(datatable)
        }else if(this.props.kode == "7.10.2"){
            this.get_visua_7_10_2(datatable)
        }else if(this.props.kode == "7.11.1"){
            this.get_visua_7_11_1(datatable)
        }else if(this.props.kode == "7.12.1"){
            this.get_visua_7_12_1(datatable)
        }else if(this.props.kode == "7.12.2"){
            this.get_visua_7_12_2(datatable)
        }else if(this.props.kode == "8.1.1"){
            this.get_visua_8_1_1(datatable)
        }else if(this.props.kode == "8.1.2"){
            this.get_visua_8_1_2(datatable)
        }else if(this.props.kode == "8.1.3"){
            this.get_visua_8_1_3(datatable)
        }else if(this.props.kode == "8.2.1"){
            this.get_visua_8_2_1(datatable)
        }else if(this.props.kode == "8.3.1"){
            this.get_visua_8_3_1(datatable)
        }else if(this.props.kode == "8.4.1"){
            this.get_visua_8_4_1__8_4_2__8_4_3(datatable)
        }else if(this.props.kode == "8.4.2"){
            this.get_visua_8_4_1__8_4_2__8_4_3(datatable)
        }else if(this.props.kode == "8.4.3"){
            this.get_visua_8_4_1__8_4_2__8_4_3(datatable)
        }else if(this.props.kode == "9.1.1"){
            this.get_visua_9_1_1(datatable)
        }else if(this.props.kode == "9.1.2"){
            this.get_visua_9_1_2(datatable)
        }else if(this.props.kode == "9.2.1"){
            this.get_visua_9_2_1__9_2_2__9_2_3(datatable)
        }else if(this.props.kode == "9.2.2"){
            this.get_visua_9_2_1__9_2_2__9_2_3(datatable)
        }else if(this.props.kode == "9.2.3"){
            this.get_visua_9_2_1__9_2_2__9_2_3(datatable)
        }else if(this.props.kode == "9.3.1"){
            this.get_visua_9_3_1(datatable)
        }else if(this.props.kode == "9.3.2"){
            this.get_visua_9_3_2(datatable)
        }else if(this.props.kode == "9.4.1"){
            this.get_visua_9_4_1(datatable)
        }else if(this.props.kode == "9.4.2"){
            this.get_visua_9_4_2(datatable)
        }else if(this.props.kode == "9.5.1"){
            this.get_visua_9_5_1(datatable)
        }else if(this.props.kode == "9.5.2"){
            this.get_visua_9_5_2(datatable)
        }else if(this.props.kode == "9.6.1"){
            this.get_visua_9_6_1(datatable)
        }else if(this.props.kode == "9.6.2"){
            this.get_visua_9_6_2(datatable)
        }else if(this.props.kode == "9.6.3"){
            this.get_visua_9_6_3(datatable)
        }else if(this.props.kode == "9.7.1"){
            this.get_visua_9_7_1(datatable)
        }else if(this.props.kode == "9.7.2"){
            this.get_visua_9_7_2(datatable)
        }else if(this.props.kode == "9.8.1"){
            this.get_visua_9_8_1(datatable)
        }else if(this.props.kode == "9.9.1"){
            this.get_visua_9_9_1(datatable)
        }else if(this.props.kode == "9.10.1"){
            this.get_visua_9_10_1(datatable)
        }else if(this.props.kode == "9.10.2"){
            this.get_visua_9_10_2(datatable)
        }else if(this.props.kode == "9.11.1"){
            this.get_visua_9_11_1(datatable)
        }else if(this.props.kode == "9.11.2"){
            this.get_visua_9_11_2(datatable)
        }else if(this.props.kode == "9.12.1"){
            this.get_visua_9_12_1(datatable)
        }else if(this.props.kode == "9.12.2"){
            this.get_visua_9_12_2(datatable)
        }
    
    }
    


    get_visua_1_1_1=(datatable)=>{
        this.setState({datane:[]}) 
        let label = []
        let value = []
        let colour = []
        //let ungu = "#844dc2"
        datatable.map((result)=>{
            label.push(result.kecamatan)
            value.push(result.luas_km_2)

            let random = randomColor();
            colour.push(random)

        })
        // //console.log(label)
        // //console.log(value)
        const datane = {
            labels: label,
            datasets: [
              {
                label: this.props.sub_kategori,
                backgroundColor: colour,
                borderColor: colour,
                borderWidth: 1,
                hoverBackgroundColor: colour,
                hoverBorderColor: colour,
                data: value
              }
            ]
          };
       this.setState({datane:datane})   
       
    }
    get_visua_1_1_2__1_1_3__1_1_4=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let colour = []
        datatable.map((result)=>{
            label.push(result.kelurahan)
            value.push(result.luas_km_2)
        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        const datane = {
            labels: label,
            datasets: [{
              label:this.props.sub_kategori,  
              data: value,
              backgroundColor: colour,
              hoverBackgroundColor: colour
            }]
          };
       this.setState({datane:datane}) 
    }
    get_visua_1_2_1__1_2_2__1_2_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan)
            value.push(result.jarak_ke_kantor_kecamatan)
            value1.push(result.jarak_ke_balaikota)
        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jarak ke kantor kecamatan',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jarak ke kantor balai kota',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane}) 
    }
    get_visua_1_3_1__1_3_2__1_3_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan)
            value.push(result.ketinggian_tempat_m)
            value1.push(result.kedalaman_sumur_m)
        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'ketinggian tempat dlm meter',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'kedalaman sumur dlm meter',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane}) 
    }
    get_visua_1_4_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_sungai)
            value.push(result.panjang_km)
            value1.push(result.debit_maximum_m_3_per_detik)
            value2.push(result.debit_minimum_m_3_per_detik)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'panjang_km',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'debit_maksimum_m_3_per_detik',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'debit_minimum_m_3_per_detik',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane}) 
    }
    get_visua_2_1_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.kelurahan)
            value1.push(result.rt)
            value2.push(result.rw)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'kelurahan',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'rt',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'rw',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_2_1_2__2_1_3__2_1_4=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value1.push(result.rt)
            value2.push(result.rw)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'rt',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'rw',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_2_2_1__2_2_2__2_2_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value1.push(result.hansip)
            value2.push(result.pencatat_nikah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'hansip',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'pencatat_nikah',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_3_1_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelompok_umur+'('+result.tahun+')')
            value.push(result.L)
            value1.push(result.P)
            value2.push(result.jumlah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'laki-laki',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'perempuan',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_3_2_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.L)
            value1.push(result.P)
            value2.push(result.jumlah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'laki-laki',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'perempuan',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_3_2_2__3_2_3__3_2_4=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jenis_kelamin_laki2)
            value1.push(result.jenis_kelamin_perempuan)
            value2.push(result.rasio_jenis_kelamin)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'laki-laki',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'perempuan',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_3_3_1__3_3_2__3_3_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.persentase_penduduk)
            value1.push(result.kepadatan_penduduk_per_km2)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'persentase penduduk',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'kepadatan penduduk per km2',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },]
          };
       this.setState({datane:datane})
    }
    get_visua_3_4_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.lahir)
            value1.push(result.datang)

        })
        console.log(datatable)
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'lahir',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'datang',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_3_5_1__3_5_2__3_5_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jumlah_keluarga)
            value1.push(result.persentase)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'persentase penduduk',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'kepadatan penduduk per km2',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },]
          };
       this.setState({datane:datane})
    }
    get_visua_3_6_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.bulan+'('+result.tahun+')')
            value.push(result.kelahiran)
            value1.push(result.kematian)
            value2.push(result.perkawinan)
            value3.push(result.perceraian)
        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'kelahiran',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'kematian',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'perkawinan',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'perceraian',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_3_7_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.islam)
            value1.push(result.protestan)
            value2.push(result.katholik)
            value3.push(result.hindu)
            value4.push(result.budha)
            value5.push(result.lainnya)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'islam',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'protestan',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'katholik',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'hindu',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'budha',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'lainnya',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_3_7_2__3_7_3__3_7_4=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.islam)
            value1.push(result.protestan)
            value2.push(result.katholik)
            value3.push(result.hindu)
            value4.push(result.budha)
            value5.push(result.lainnya)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'islam',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'protestan',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'katholik',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'hindu',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'budha',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'lainnya',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_1_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.jumlah_sekolah_tk)
            value1.push(result.jumlah_guru_tk)
            value2.push(result.jumlah_murid_tk)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah sekolah tk',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah guru tk',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah murid tk',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_2_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.jumlah_sd)
            value1.push(result.jumlah_guru_sd)
            value2.push(result.jumlah_murid_sd)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah sd',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah guru sd',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah murid sd',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_3_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.jumlah_smp)
            value1.push(result.jumlah_guru_smp)
            value2.push(result.jumlah_murid_smp)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah smp',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah guru smp',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah murid smp',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_4_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.jumlah_sma)
            value1.push(result.jumlah_guru_sma)
            value2.push(result.jumlah_murid_sma)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah sma',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah guru sma',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah murid sma',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_5_1__4_5_2__4_5_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jumlah_smk_negeri)
            value1.push(result.jumlah_smk_swasta)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah smk negeri',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah smk swasta',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_6_1__4_6_2__4_6_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jumlah_ptn)
            value1.push(result.jumlah_pts)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah ptn',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah pts',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_7_1__4_7_2__4_7_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jumlah_slb_negeri)
            value1.push(result.jumlah_slb_swasta)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah slb negeri',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah slb swasta',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_8_1__4_8_2__4_8_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jumlah_ponpes_atau_madrasah_diniyah)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah ponpes atau madrasah diniyah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_10_1__4_10_2__4_10_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jumlah_mi_negeri)
            value1.push(result.jumlah_mi_swasta)
            value2.push(result.jumlah_mts_negeri)
            value3.push(result.jumlah_mts_swasta)
            value4.push(result.jumlah_ma_negeri)
            value5.push(result.jumlah_ma_swasta)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah mi negeri',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah mi swasta',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah mts negeri',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah mts swasta',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah ma negeri',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah ma swasta',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_11_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.tahun)
            value.push(result.pengunjung_perpustakaan_laki_laki)
            value1.push(result.pengunjung_perpustakaan_perempuan)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'pengunjung perpustakaan laki laki',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'pengunjung perpustakaan perempuan',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_11_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let value6 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.tahun)
            value.push(result.pelajar)
            value1.push(result.mahasiswa)
            value2.push(result.pns)
            value3.push(result.swasta)
            value4.push(result.wiraswasta)
            value5.push(result.umum)
            value6.push(result.santri)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'pelajar',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'mahasiswa',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'pns',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'swasta',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'wiraswasta',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'umum',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'santri',  
                        data: value6,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_11_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let value6 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.tahun)
            value.push(result.pelajar)
            value1.push(result.mahasiswa)
            value2.push(result.pns)
            value3.push(result.swasta)
            value4.push(result.wiraswasta)
            value5.push(result.umum)
            value6.push(result.santri)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'pelajar',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'mahasiswa',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'pns',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'swasta',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'wiraswasta',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'umum',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'santri',  
                        data: value6,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_11_4=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let value6 = []
        let value7 = []
        let value8 = []
        let value9 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.tahun)
            value.push(result.umum)
            value1.push(result.filsafat)
            value2.push(result.agama)
            value3.push(result.sosial)
            value4.push(result.bahasa)
            value5.push(result.ilmu_murni)
            value6.push(result.ilmu_terapan)
            value7.push(result.olahraga_kesehatan)
            value8.push(result.sastra_fiksi)
            value9.push(result.sejarah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'umum',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'filsafat',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'agama',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'sosial',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'bahasa',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'ilmu murni',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'ilmu terapan',  
                        data: value6,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'olahraga dan kesehatan',  
                        data: value7,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'sastra fiksi',  
                        data: value8,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'sejarah',  
                        data: value9,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_4_11_5=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.tahun)
            value.push(result.paud)
            value1.push(result.tk)
            value2.push(result.sd)
            value3.push(result.smp)
            value4.push(result.sma)
            value5.push(result.umum)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'paud',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'tk',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'sd',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'smp',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'sma',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'umum',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_1_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let value6 = []


        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.rs)
            value1.push(result.rs_bersalin)
            value2.push(result.puskesmas)
            value3.push(result.puskesmas_pembantu)
            value4.push(result.apotek)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah rs',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah rs bersalin',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah puskesmas',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah puskesmas pembantu',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah apotek',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_1_2__5_1_3__5_1_4=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let value6 = []
        let value7 = []
        let value8 = []
        let value9 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.rs)
            value1.push(result.rs_bersalin)
            value2.push(result.puskesmas)
            value3.push(result.puskesmas_pembantu)
            value4.push(result.apotek)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah rs',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah rs bersalin',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah puskesmas',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah puskesmas pembantu',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah apotek',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_2_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.pasangan_usia_subur)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah pasangan usia subur',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_3_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let value6 = []

        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.IUD)
            value1.push(result.MOW)
            value2.push(result.MOP)
            value3.push(result.Kondom)
            value4.push(result.Implan)
            value5.push(result.Suntik)
            value6.push(result.Pil)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'IUD',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'MOW',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'MOP',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Kondom',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Implan',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Suntik',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Pil',  
                        data: value6,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah pasangan usia subur',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },]
          };
       this.setState({datane:datane})
    }
    get_visua_5_4_1__5_4_2__5_4_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jumlah_mck_umum)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah mck umum',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_5_1__5_5_2__5_5_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jumlah_tempat_pembuangan_sampah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah tempat pembuangan sampah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_6_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.Bayi_Lahir)
            value1.push(result.BBLR)
            value2.push(result.BBLR_Dirujuk)
            value3.push(result.Gizi_Buruk)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'Bayi Lahir',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'BBLR',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'BBLR Dirujuk',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Gizi Buruk',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_6_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let value6 = []
        let value7 = []
        let value8 = []
        let value9 = []
        let value10 = []
        let value11 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.BCG)
            value1.push(result.DPT1)
            value2.push(result.DPT2)
            value3.push(result.DPT3)
            value4.push(result.Campak)
            value5.push(result.Polio1)
            value6.push(result.Polio2)
            value7.push(result.Polio3)
            value8.push(result.Polio4)
            value9.push(result.Hepatitis_B1)
            value10.push(result.Hepatitis_B2)
            value11.push(result.Hepatitis_B3)



        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'BCG',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'DPT1',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'DPT2',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'DPT3',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Campak',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Polio 1',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Polio 2',  
                        data: value6,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Polio 3',  
                        data: value7,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Polio 4',  
                        data: value8,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Hepatitis B1',  
                        data: value9,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Hepatitis B2',  
                        data: value10,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Hepatitis B3',  
                        data: value11,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_7_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.bulan+'('+result.tahun+')')
            value.push(result.Pendonor)
            value1.push(result.Darah_didapat)
            value2.push(result.Permintaan_darah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'Jumlah Pendonor',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Jumlah Darah didapat',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Jumlah Permintaan darah',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_7_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.bulan+'('+result.tahun+')')
            value.push(result.golongan_a)
            value1.push(result.golongan_b)
            value2.push(result.golongan_ab)
            value3.push(result.golongan_o)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'Jumlah golongan darah a',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Jumlah golongan darah b',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Jumlah golongan darah ab',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Jumlah golongan darah o',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_7_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.bulan+'('+result.tahun+')')
            value.push(result.jumlah_pasien)
            value1.push(result.jumlah_darah)
            value2.push(result.jumlah_darah_dipenuhi)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'Jumlah Pasien',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Jumlah Darah',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Jumlah Darah Dipenuhi',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_8_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.bulan+'('+result.tahun+')')
            value.push(result.puskesmas_ngronggo)
            value1.push(result.puskesmas_setono_bethek)
            value2.push(result.puskesmas_balowerti)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'Puskesmas Ngronggo',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Puskesmas Setono Bethek',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'Puskesmas Balowerti',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_8_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let colour2 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.bulan+'('+result.tahun+')')
            value.push(result.puskesmas_ngletih)
            value1.push(result.puskesmas_pesantren_1)
            value2.push(result.puskesmas_pesantren_2)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour1.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour2.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        stack:'siji',
                        label:'Puskesmas Ngletih',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        stack:'siji',
                        label:'Puskesmas Pesantren 1',  
                        data: value1,
                        backgroundColor: colour1,
                        hoverBackgroundColor: colour1
                    },{
                        stack:'siji',
                        label:'Puskesmas Pesantren 2',  
                        data: value2,
                        backgroundColor: colour2,
                        hoverBackgroundColor: colour2
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_5_9_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let colour = []
        let colour1 = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.jenis_penyakit+'('+result.tahun+')')
            value.push(result.jumlah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'Jumlah kasus',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_6_1_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []

        let colour = []
        let colour1 = []
        let colour2 = []
        let colour3 = []
        let colour4 = []
        let colour5 = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.masjid)
            value1.push(result.mushola)
            value2.push(result.gereja_protestan)
            value3.push(result.gereja_katholik)
            value4.push(result.pura)
            value5.push(result.vihara)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour1.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour2.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour2.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour3.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour4.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour5.push(random)
        })
        

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah masjid',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah mushola',  
                        data: value1,
                        backgroundColor: colour1,
                        hoverBackgroundColor: colour1
                    },{
                        label:'jumlah gereja protestan',  
                        data: value2,
                        backgroundColor: colour2,
                        hoverBackgroundColor: colour2
                    },{
                        label:'jumlah gereja katholik',  
                        data: value3,
                        backgroundColor: colour3,
                        hoverBackgroundColor: colour3
                    },{
                        label:'jumlah pura',  
                        data: value4,
                        backgroundColor: colour4,
                        hoverBackgroundColor: colour4
                    },{
                        label:'jumlah vihara',  
                        data: value5,
                        backgroundColor: colour5,
                        hoverBackgroundColor: colour5
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_6_2_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.jumlah_pernikahan_tercatat_di_kementerian_agama_kota_kediri)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        
        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah pernikahan tercatat kemenag',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_6_2_2__6_2_3__6_2_4=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []

        let colour = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.surat_nikah)
            value1.push(result.surat_talak)
            value2.push(result.surat_cerai)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        
        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah surat nikah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah surat talak',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah surat cerai',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_6_3_1__6_3_2__6_3_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []

        let colour = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.panti_asuhan)
            value1.push(result.panti_jompo)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        
        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah panti asuhan',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah panti jompo',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_6_4_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.jumlah_tindak_pidana_menurut_polres_kediri_kota)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah tindak pidana',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_6_4_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.jumlah_penyelesaian_tindak_pidana_menurut_polres_kediri_kota)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah penyelesaian tindak pidana',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_6_4_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.jumlah_resiko_penduduk_tindak_pidana_per_1000_penduduk)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah resiko tindak pidana',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_6_4_4=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.selang_waktu_terjadinya_tindak_pidana_dlm_hari)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'selang waktu tindak pidana dalam hari',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_6_5_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.tahun)
            value.push(result.jumlah_penduduk_miskin_dlm_ribu)
            value1.push(result.persentase_penduduk_miskin_p0)
            value2.push(result.index_kedalaman_p1)
            value3.push(result.index_keparahan_p2)
            value4.push(result.garis_kemiskinan)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah penduduk miskin dalam ribu',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'persentase penduduk miskin',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'index kedalaman 1',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'index kedalaman 2',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'garis kemiskinan',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_1_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []

        let colour = []
        let colour1 = []
        let colour2 = []
        let colour3 = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.tahun)
            value.push(result.pasar)
            value1.push(result.toko)
            value2.push(result.kios)
            value3.push(result.warung)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour1.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour2.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour3.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        stack:'siji',
                        label:'jumlah pasar',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        stack:'siji',
                        label:'jumlah toko',  
                        data: value1,
                        backgroundColor: colour1,
                        hoverBackgroundColor: colour1
                    },{
                        stack:'siji',
                        label:'jumlah kios',  
                        data: value2,
                        backgroundColor: colour2,
                        hoverBackgroundColor: colour2
                    },{
                        stack:'siji',
                        label:'jumlah warung',  
                        data: value3,
                        backgroundColor: colour3,
                        hoverBackgroundColor: colour3
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_1_2__7_1_3__7_1_4=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let value6 = []
        let value7 = []
        let value8 = []
        let value9 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.kelompok_pertokoan)
            value1.push(result.pasar_bangunan_permanen)
            value2.push(result.pasar_bangunan_semi_permanen)
            value3.push(result.pasar_tanpa_bangunan)
            value4.push(result.minimarket_atau_swalayan)
            value5.push(result.toko_atau_warung_kelontong)
            value6.push(result.restoran_atau_rumah_makan)
            value7.push(result.warung_atau_kedai_makanan)
            value8.push(result.hotel)
            value9.push(result.hostel_motel_losmen_wisma)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah kelompok pertokoan',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah pasar bangunan permanen',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah pasar bangunan semi permanen',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah pasar tanpa bangunan',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah minimarket atau swalayan',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah toko atau warung kelontong',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah restoran atau rumah makan',  
                        data: value6,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah warung atau kedai makanan',  
                        data: value7,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah hotel',  
                        data: value8,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah hostel motel losmen wisma',  
                        data: value9,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_2_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []

        let colour = []
        let colour1 = []
        let colour2 = []
        let colour3 = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.kud)
            value1.push(result.kpri)
            value2.push(result.kopkar)
            value3.push(result.lainnya)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour1.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour2.push(random)
        })
        label.map((colou)=>{
            let random = randomColor()
            colour3.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        stack:'siji',
                        label:'jumlah kud',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        stack:'siji',
                        label:'jumlah kpri',  
                        data: value1,
                        backgroundColor: colour1,
                        hoverBackgroundColor: colour1
                    },{
                        stack:'siji',
                        label:'jumlah kopkar',  
                        data: value2,
                        backgroundColor: colour2,
                        hoverBackgroundColor: colour2
                    },{
                        stack:'siji',
                        label:'jumlah koperasi lainnya',  
                        data: value3,
                        backgroundColor: colour3,
                        hoverBackgroundColor: colour3
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_2_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.koperasi_aktif)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah koperasi aktif',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_3_1__7_3_2__7_3_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []
        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.hotel_atau_penginapan)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        
        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah panti asuhan',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_4_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.restoran_atau_rumah_makan)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah restaurant atau rumah makan',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_5_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.irigasi)
            value1.push(result.non_irigasi)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah irigasi',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah non irigasi',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_6_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.padi_sawah)
            value1.push(result.jagung)
            value2.push(result.kedelai)
            value3.push(result.kacang_tanah)
            value4.push(result.ubi_kayu)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'luas padi sawah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'luas jagung',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'luas kedelai',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'luas kacang tanah',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'luas ubi kayu',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_6_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.padi_sawah)
            value1.push(result.jagung)
            value2.push(result.kedelai)
            value3.push(result.kacang_tanah)
            value4.push(result.ubi_kayu)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah padi sawah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah jagung',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah kedelai',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah kacang tanah',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah ubi kayu',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_7_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.jenis_sayuran +' '+result.kecamatan+'('+result.tahun+')')
            value1.push(result.luas_panen_Ha)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'luas panen dlm Ha',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_7_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.jenis_sayuran +' '+result.kecamatan+'('+result.tahun+')')
            value.push(result.produksi_Ton)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah produksi dlm Ton',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_8_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.jenis_buah +' '+result.kecamatan+'('+result.tahun+')')
            value.push(result.produksi_Ton)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah produksi dlm Ton',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_9_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.kelapa)
            value1.push(result.tebu)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah produksi kelapa dlm (Ha)',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah produksi tebu dlm (Ha)',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }

    get_visua_7_10_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.sapi_perah)
            value1.push(result.sapi_potong)
            value2.push(result.kerbau)
            value3.push(result.kuda)
            value4.push(result.kambing)
            value5.push(result.domba)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'populasi sapi perah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'populasi sapi potong',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'populasi kerbau',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'populasi kuda',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'populasi kambing',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'populasi domba',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_10_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.ayam_kampung)
            value1.push(result.ayam_petelur)
            value2.push(result.ayam_pedaging)
            value3.push(result.itik_atau_itik_manila)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'populasi ayam kampung',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'populasi ayam petelur',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'populasi ayam pedaging',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'populasi itik atau itik manila',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_11_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.kambing)
            value1.push(result.domba)
            value2.push(result.babi)
            value3.push(result.sapi_potong)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah kambing dipotong',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah domba dipotong',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah babi dipotong',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah sapi dipotong',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_12_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []
        let value4 = []
        let value5 = []
        let value6 = []
        let value7 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.sapi_potong)
            value1.push(result.kambing)
            value2.push(result.domba)
            value3.push(result.babi)
            value4.push(result.ayam_kampung)
            value5.push(result.ayam_petelur)
            value6.push(result.ayam_pedaging)
            value7.push(result.itik_atau_itik_manila)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'produksi daging sapi potong',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging kambing',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging domba',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging babi',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging ayam kampung',  
                        data: value4,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging ayam petelur',  
                        data: value5,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging ayam pedaging',  
                        data: value6,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging itik atau itik manila',  
                        data: value7,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_7_12_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.telur_ayam_kampung)
            value1.push(result.telur_ayam_petelur)
            value2.push(result.telur_itik)
            value3.push(result.susu_sapi_perah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'produksi daging sapi potong',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging kambing',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging domba',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'produksi daging babi',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_8_1_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []

        let colour = []
        let colour1 = []
        let colour2 = []
        let colour3 = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.negara)
            value1.push(result.provinsi)
            value2.push(result.kota)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'panjang jalan nasional dlm km',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'panjang jalan provinsi dlm km',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'panjang jalan kota dlm km',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_8_1_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []

        let colour = []
        let colour1 = []
        let colour2 = []
        let colour3 = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.aspal)
            value1.push(result.tidak_aspal)
            value2.push(result.lainnya)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'panjang jalan aspal',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'panjang jalan tidak aspal',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'panjang jalan lainnya',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_8_1_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.baik)
            value1.push(result.sedang)
            value2.push(result.rusak)
            value3.push(result.rusak_berat)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'panjang jalan kondisi baik',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'panjang jalan kondisi sedang',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'panjang jalan kondisi rusak',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'panjang jalan kondisi rusak berat',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_8_2_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.mobil_penumpang)
            value1.push(result.bus)
            value2.push(result.truk)
            value3.push(result.sepeda_motor)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah mobil penumpang',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah bus',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah truk',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah sepeda motor',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_8_3_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kecamatan+'('+result.tahun+')')
            value.push(result.jumlah_kantor_pos_di_kota_kediri)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'kantor pos di kota kediri',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_8_4_1__8_4_2__8_4_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.jumlah_menara_telepon_seluler_bts)
            value1.push(result.jumlah_operator_layanan_komunikasi_telepon_seluler_yang_menjangk)
            value2.push(result.kondisi_sinyal_telepon_seluler_sebagian_besar_wilayah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah menara telepon seluler bts',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'jumlah operator layanan komunikasi telepon seluler',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'kondisi sinyal telepon seluler sebagian besar wilayah',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_1_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.jenis_pendapatan+' '+result.sub_jenis_pendapatan+'('+result.tahun+')')
            value.push(result.realisasi_pendapatan_dlm_riburupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'realisasi pendapatan dlm ribu rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_1_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.jenis_belanja+' '+result.sub_jenis_belanja+'('+result.tahun+')')
            value.push(result.realisasi_belanja_dlm_riburupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'realisasi belanja dlm ribu rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_2_1__9_2_2__9_2_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kelurahan+'('+result.tahun+')')
            value.push(result.target_dalam_rupiah)
            value1.push(result.realisasi_dalam_rupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })
        

        const datane = {
            labels: label,
            datasets: [{
                        label:'target dlm rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'realisasi dlm rupiah',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_3_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kelompok_makanan+'('+result.tahun+')')
            value.push(result.rata_rata_pengeluaran)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'rata rata pengeluaran per kapita',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_3_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.kelompok_bukan_makanan+'('+result.tahun+')')
            value.push(result.rata_rata_pengeluaran)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'rata rata pengeluaran per kapita menurut kelompok bukan makanan',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_4_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.lapangan_usaha+'('+result.tahun+')')
            value.push(result.pdrb_harga_berlaku_dlm_miliar_rupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'pdrb harga berlaku dlm milyar rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_4_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.lapangan_usaha+'('+result.tahun+')')
            value.push(result.pdrb_harga_konstan_2010_dlm_miliar_rupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'pdrb harga konstan 2010`s dlm milyar rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_5_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.jenis_pengeluaran+'('+result.tahun+')')
            value.push(result.pdrb_harga_berlaku_dlm_miliar_rupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'pdrb harga berlaku dlm milyar rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_5_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.jenis_pengeluaran+'('+result.tahun+')')
            value.push(result.pdrb_harga_konstan_2010_dlm_miliar_rupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'pdrb harga konstan 2010`s dlm milyar rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_6_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.tahun)
            value.push(result.angka_harapan_hidup_dlm_tahun)
            value1.push(result.harapan_lama_sekolah_dlm_persen)
            value2.push(result.rata2_lama_sekolah_dlm_tahun)
            value3.push(result.pengeluaran_perkapita_riil_disesuaikan_dlm_riburupiah)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'angka harapan hidup dlm tahun',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'harapan lama sekolah dlm persen',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'rata" lama sekolah dlm tahun',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'pengeluaran per kapita riil disesuaikan dlm ribu rupiah',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_6_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []
        let value1 = []
        let value2 = []
        let value3 = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.tahun)
            value.push(result.indeks_kesehatan)
            value1.push(result.indeks_pendidikan)
            value2.push(result.indeks_ppp)
            value3.push(result.ipm)


        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'index kesehatan',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'index pendidikan',  
                        data: value1,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'index ppp',  
                        data: value2,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    },{
                        label:'ipm',  
                        data: value3,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_6_3=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.perbandingan_indeks_pembangunan_manusia)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'perbandingan indeks pembangunan manusia',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_7_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.tingkat_partisipasi_angkatan_kerja_daerah_dalam_persen)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'tingkat partisipasi angkatan kerja daerah dalam persen',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_7_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.tingkat_pengangguran_terbuka_dalam_persen)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'tingkat pengangguran terbuka dalam persen',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_8_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.jumlah_penduduk_beberapa_daerah_dalam_ribu)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah penduduk beberapa daerah dalam ribu',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_9_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.jumlah_penduduk_miskin_beberapa_daerah_dalam_ribu)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah penduduk miskin beberapa daerah dalam ribu',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_10_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.jumlah_dlm_milyar_rupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah dlm milyar rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_10_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.jumlah_dlm_milyar_rupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah dlm milyar rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_11_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.jumlah_dlm_ribu_rupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah dlm ribu rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_11_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.jumlah_dlm_ribu_rupiah)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'jumlah dlm ribu rupiah',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_12_1=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.jenis_lapangan_usaha+'('+result.tahun+')')
            value.push(result.besaran_dlm_persen)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'besaran dlm persen',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }
    get_visua_9_12_2=(datatable)=>{
        this.setState({datane:[]})     
        let label = []
        let value = []

        let colour = []

        let labil = []
        datatable.map((result)=>{
            label.push(result.nama_daerah+'('+result.tahun+')')
            value.push(result.besaran_dlm_persen)

        })
        //console.log(label)
        //console.log(value)
        label.map((colou)=>{
            let random = randomColor()
            colour.push(random)
        })

        const datane = {
            labels: label,
            datasets: [{
                        label:'besaran dlm persen',  
                        data: value,
                        backgroundColor: colour,
                        hoverBackgroundColor: colour
                    }]
          };
       this.setState({datane:datane})
    }

    to_IMG=(sub_kategori)=>{
        const canvasSave = document.getElementById('stackID');
       canvasSave.toBlob(function (blob) {
           saveAs(blob, sub_kategori)
       })
    }

    get_year=()=>{
        const datatable = this.props.prod.datatable
        let thn = []
        thn.push({'tahun':'pilih tahun'})

        datatable.map((io)=>{
            if(io.hasOwnProperty('tahun')){
                thn.push({'tahun':io.tahun})
            }
        })
        const thn_bersih = _.uniqWith(thn,_.isEqual)

        const thn_start = thn_bersih.sort((a,b)=>
             b.tahun - a.tahun
        )
        const thn_end = thn_bersih.sort((a,b)=>
             b.tahun - a.tahun
        )
        this.setState({thn_start:thn_start,thn_end:thn_end})

    }

    filtre=()=>{
        const {start,end}= this.state
        let data = this.props.prod.datatable
        let filtrez = this.props.prod.datatable_filter
        let filt=data.filter(dt => {
            return dt.tahun >= start && dt.tahun <= end
        })
        console.log(filt)
        console.log(filtrez)
        this.props.clear_datatable_filter()
        this.props.add_to_datatable_filter(filt)
        
        this.which(filtrez)

    }

    render(){
        const {datane,thn_start,start} = this.state
        const {sub_kategori} = this.props
        //console.log(datane)
        return(
            <div class="col">
                            <div class="row justify-content-start mb-3">
                                <h4 style={{color:'#fff'}}>Visual {sub_kategori}</h4>
                            </div> 
                        {(thn_start.length != 1)?   
                            <div class="row justify-content-start mb-3">
                                <text style={{color:'white',marginRight:5,fontSize:20}}>Mau filter tahun ?</text>
                                <select class="form-control" value={this.state.start} onChange={event => this.setState({start: event.target.value})} style={{width:130,marginRight:15}}>
                                    {thn_start.map((ts)=>{
                                        return(<option value={ts.tahun}>{ts.tahun}</option>)
                                    })}
                                </select>
                                <select class="form-control" value={this.state.end} onChange={event => this.setState({end: event.target.value})} style={{width:130,marginRight:15}}>
                                    {thn_start.map((ts)=>{
                                        return(<option value={ts.tahun}>{ts.tahun}</option>)
                                    })}
                                </select>
                                <div role="button" onClick={()=>this.filtre()}>
                                    <text style={{color:'#fff',fontSize:20}}>terapkan</text>
                                </div>
                            </div>
                            :
                            null
                        }
                            <div class="row justify-content-end">
                                <div role="button" style={{color:'#fff',marginBottom:15}} onClick={()=>this.to_IMG(sub_kategori)}>
                                    download as gambar
                                </div>   
                            </div>
                            
                {(this.props.kode == "1.1.1")?
                <div>
                    <div> <Doughnut id="stackID" ref={(reference) =>this.chartRef = reference} data={datane} /></div>  
                </div>    
                    :
                  (this.props.kode == "1.1.2" )?
                    <div> <Doughnut id="stackID" ref={(reference) =>this.chartRef = reference} data={datane} /></div>  
                    :
                  (this.props.kode == "1.1.3")?
                    <div> <Doughnut id="stackID" ref={(reference) =>this.chartRef = reference} data={datane} /></div>  
                    :
                  (this.props.kode == "1.1.4")?
                    <div> <Doughnut id="stackID" ref={(reference) =>this.chartRef = reference} data={datane} /></div>  
                    :
                (this.props.kode == "1.2.1")?
                    <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                          
                    </div>    
                :(this.props.kode == "1.2.2")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                              
                </div>    
                :(this.props.kode == "1.2.3")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "1.3.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                                      
                </div>    
                :(this.props.kode == "1.3.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "1.3.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "1.4.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "2.1.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true,
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "2.1.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "2.1.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "2.1.4")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "2.2.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "2.2.2")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "2.2.3")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.1.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.2.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.2.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.2.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.2.4")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.3.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.3.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.3.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.4.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.5.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.5.2")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.5.3")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.6.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.7.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.7.2")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.7.3")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "3.7.4")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.1.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.2.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.3.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.4.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.5.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.5.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.5.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.6.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.6.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.6.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.7.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.7.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.7.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.8.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.8.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.8.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                        
                </div>    
                :(this.props.kode == "4.10.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "4.10.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "4.10.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>  
                :(this.props.kode == "4.11.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "4.11.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>
                :(this.props.kode == "4.11.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "4.11.4")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "4.11.5")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.1.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.1.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.1.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.1.4")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.2.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.3.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.4.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.4.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.4.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.5.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.5.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.5.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.6.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.6.2")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.7.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.7.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.7.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.8.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.8.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "5.9.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.1.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.2.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.2.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.2.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.2.4")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.3.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.3.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.3.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.4.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.4.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.4.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.4.4")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "6.5.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.1.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.1.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.1.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.1.4")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.2.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.2.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.3.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.3.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.3.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.4.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.5.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.6.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.6.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.7.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.7.2")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.8.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.9.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.10.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.10.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.11.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.12.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "7.12.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "8.1.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "8.1.2")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "8.1.3")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "8.2.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "8.3.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "8.4.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "8.4.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "8.4.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.1.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.1.2")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.2.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.2.2")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.2.3")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.3.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.3.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.4.1")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.4.2")?
                <div>
                    <HorizontalBar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.5.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.5.2")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.6.1")?
                <div>
                    <Line
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.6.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.6.3")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.7.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.7.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.8.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.9.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.10.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.10.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.11.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.11.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.12.1")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :(this.props.kode == "9.12.2")?
                <div>
                    <Bar
                        id="stackID"
                        ref={(reference) =>this.chartRef = reference}
                        data={datane}
                        width={50}
                        height={25}
                        options={{
                            maintainAspectRatio: true
                        }}
                        />
                    
                </div>    
                :         
                    null
                }
            </div>
        )
    }
}

let ausiV = Fuck(Visua)
export default ausiV;