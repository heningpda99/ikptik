import React from 'react'

class Copyright extends React.Component{
    constructor(){
        super();

    }

    render(){
        return(
            <div class="copyright py-4 text-center text-white">
                <div class="container">
                    <i class="fa fa-eye" style={{marginRight:5, fontSize:10,marginTop:3}} />
                    <text style={{fontSize:10,marginRight:15}}>{this.props.show_visit}</text>
                    
                    <i class="fa fa-download" style={{marginRight:5,fontSize:10}}/>
                    <text style={{marginRight:20,fontSize:10}}>{this.props.show_download}</text>

                    <a href="https://diskominfo.kedirikota.go.id/">
                    <small style={{color:"#fff"}}>supported by Dinas Komunikasi dan Informatika Kota Kediri 2021</small>
                    </a>
                    <img src={process.env.PUBLIC_URL+"assets/img/kmnf.png"} style={{marginLeft:5,width:20,height:20}} alt="" />
                </div>
            </div>
        )
    }
}

export default Copyright;