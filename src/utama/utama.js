import React from 'react'
import loadable from '@loadable/component'
import {Fuck} from '../allseeing';
import {banner,coba} from './banner'
import { Link } from 'react-router-dom';
import axios from 'axios'
let moment = require('moment')

const Atas = loadable(()=> import('../header/atas')) 
const Header = loadable(()=> import('../header/header'))
const Nav = loadable(()=> import('../nav/nav'))
const Footer = loadable(()=> import('../footer/footer'))
const Modals = loadable(()=> import('../footer/modals'))
const Bawah = loadable(()=> import('../footer/bawah'))
const Copyright = loadable(()=> import('../footer/copyright'))

const menu = [{
              lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_geografis.png",
              to:"/Sub",
              id:1
            },{
                lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_pemerintahan.png",
              to:"/Sub",
              id:2
            },{
                lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_kependudukan.png",
              to:"/Sub",
              id:3
            },{
                lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_pendidikan.png",
              to:"/Sub",
              id:4
            },{
                lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_kesehatan.png",
              to:"/Sub",
              id:5
            },{
                lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_agasos.png",
              to:"/Sub",
              id:6
            },{
                lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_perdagangan_1.png",
              to:"/Sub",
              id:7
            },{
                lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_transkom.png",
              to:"/Sub",
              id:8
            },{
                lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_keuangan.png",
              to:"/Sub",
              id:9
            },{
                lebar: "col-md-6 col-lg-4 mb-5",
              img: process.env.PUBLIC_URL+"/assets/img/portfolio/icon_satudata.png",
              to:"/SDI",
              id:10
             
            }
            ];


class Utama extends React.PureComponent{
    constructor(){
        super();
        this.state = {
            img:[],
            menu:[],
            show_download:0,
            show_visit:0
        }

    }

    componentDidMount(){
        this.setState({menu:menu})
        this.show_who_visit_general()
        this.show_who_download_general()
    }
    show_who_visit_general=()=>{
        let url = this.props.reds.url.UerEls+`api/end_show_visit_general/`
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
            },
            params:{
                tahun:moment().format('YYYY'),
                bulan:moment().format('MM')
            }
        }).then((response)=>{
                //console.log(response.data[0])
                if(response.data != 0){
                    this.setState({show_visit:response.data[0].jumlah_bln_ini})
                    // console.log(response.data)
                }else{
                    this.setState({show_visit:0})
                }
        })
    } 
    show_who_download_general=()=>{
        let url = this.props.reds.url.UerEls+`api/end_show_download_general/`
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
            },
            params:{
                tahun:moment().format('YYYY'),
                bulan:moment().format('MM')
            }
        }).then((response)=>{
                //console.log(response.data[0])
                if(response.data != 0){
                    this.setState({show_download:response.data[0].jumlah_bln_ini})
                    //console.log(response.data)
                }else{
                    this.setState({show_download:0})
                }
        })
    }  
    render(){
        const {img,show_visit,show_download} = this.state               
        return(
            <html lang="en">
                <Atas />
                <body id="page-top">
                    <Nav />
                    <Header img={coba} />
                    <section class="page-section portfolio" id="kategori" style={{backgroundColor:'	#fff4f3'}}>
                        <div class="container" >
                            <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Kategori</h2>
                            <div class="divider-custom">
                                <div class="divider-custom-line"></div>
                                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                <div class="divider-custom-line"></div>
                            </div>
                            {/* <div class="row justify-content-center"> */}
                            <div class="row justify-content-left" >
                             {menu.map((item)=>{
                                 return(
                                    
                                    <div class={item.lebar}>
                                        <Link to={{pathname:item.to , state:{id_kategori:JSON.stringify(item.id)}}}>
                                        <a href={item.a}>
                                        <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal1">
                                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                                <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                                            </div>
                                            <img class="img-fluid" src={item.img} alt="" />
                                        </div>
                                        </a>
                                        </Link>
                                    </div>
                                     
                                 )
                             })}   
                                
                            </div>
                        </div>
                    </section>
                    <section class="page-section text-white mb-0" id="download" style={{backgroundColor:'#fff'}}>
                        <div class="container">
                                <h2 class="page-section-heading text-center text-uppercase" style={{color:'#be88e0'}}>Download</h2>
                                <h3 class="text-center text-uppercase" style={{color:'#be88e0'}}>Masih Kosong Lur :D</h3>
                        </div>        
                    </section>
                    <section class="page-section text-white mb-0" id="about" style={{backgroundColor:'#be88e0'}}>
                        {/* <div style={{backgroundImage:"url("+process.env.PUBLIC_URL+`/assets/img/parang.jpg`+")" , width:'100%'}}> */}
                        <div class="container">
                            <div class="row justify-content-center mb-4">
                                <img src={process.env.PUBLIC_URL+"assets/img/icon_about/harmoni_kediri_trans_cropped.png"} style={{width:'13%',height:'13%'}} />
                                <img src={process.env.PUBLIC_URL+"assets/img/icon_about/djojo_ing_bojo.png"} style={{width:'20%',height:'20%'}} />
                                <img src={process.env.PUBLIC_URL+"assets/img/icon_about/service_city_trans.png"} style={{width:'20%',height:'20%' ,marginTop:50}} />
                            </div>
                            {/* <h2 class="page-section-heading text-center text-uppercase text-white">About</h2>
                            <div class="divider-custom divider-light">
                                <div class="divider-custom-line"></div>
                                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                <div class="divider-custom-line"></div>
                            </div> */}
                            <div class="row">
                                <div class="col-lg-4 ml-auto"><p class="lead">Statistik Sektoral adalah statistik yang pemanfaatannya ditujukan untuk memenuhi kebutuhan instansi tertentu dalam rangka penyelenggaraan tugas-tugas pemerintahan dan pembangunan yang merupakan tugas pokok instansi yang bersangkutan (UU No. 16 Tahun 1997). Statistik Sektoral diselenggarakan oleh masing-masing sektor seperti K/L/D/I</p></div>
                                <div class="col-lg-4 ml-auto"><p class="lead">Setiap instansi pemerintah daerah dalam hal ini Organisasi Pemerintah Daerah (OPD) bertanggung jawab pada pelakasanaan statistik sektoral secara mandiri dan/atau bekerjasama dengan BPS selaku Pembina Data serta melibatkan Dinas Komunikasi dan Informatika selaku Walidata</p></div>
                                <div class="col-lg-4 mr-auto"><p class="">Portal Satu Data Kota Kediri merupakan portal resmi data statistik sektoral terbuka Kota Kediri yang dikelola oleh Dinas Komunikasi dan Informatika Kota Kediri. Seluruh kumpulan data yang tersedia di Portal Satu Data Kota Kediri merupakan data publik yang tidak bersifat rahasia untuk pemenuhan kebutuhan data publik bagi masyarakat serta salah satu bentuk sinkronisasi pembangunan pemerintah pusat dengan daerah.</p></div>
                            </div>
                            {/* <div class="text-center mt-4">
                                <a class="btn btn-xl btn-outline-light" href="https://startbootstrap.com/theme/freelancer/">
                                    <i class="fas fa-download mr-2"></i>
                                    Free Download!
                                </a>
                            </div> */}
                        </div>
                        {/* </div> */}
                    </section>
                    {/* <section class="page-section" id="contact">
                        <div class="container">
                            <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Me</h2>
                            <div class="divider-custom">
                                <div class="divider-custom-line"></div>
                                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                <div class="divider-custom-line"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 mx-auto">
                                    <form id="contactForm" name="sentMessage" novalidate="novalidate">
                                        <div class="control-group">
                                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                                <label>Name</label>
                                                <input class="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name." />
                                                <p class="help-block text-danger"></p>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                                <label>Email Address</label>
                                                <input class="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address." />
                                                <p class="help-block text-danger"></p>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                                <label>Phone Number</label>
                                                <input class="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number." />
                                                <p class="help-block text-danger"></p>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                                <label>Message</label>
                                                <textarea class="form-control" id="message" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."></textarea>
                                                <p class="help-block text-danger"></p>
                                            </div>
                                        </div>
                                        <br />
                                        <div id="success"></div>
                                        <div class="form-group"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Send</button></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section> */}
                    
                    {/* <Footer /> */}
                    <Copyright show_visit={show_visit} show_download={show_download} />
                    {/* <Modals /> */}
                    <Bawah />
                </body>
            </html>
        )
    }
}
let amatU = Fuck(Utama)
export default amatU;