import React from 'react'
import RBCarousel from "react-bootstrap-carousel";
import "react-bootstrap-carousel/dist/react-bootstrap-carousel.css";

class Header extends React.Component{
    constructor(){
        super();
        this.slider = React.createRef();
        this.state = {
            im:[]
        }

    }
    componentDidMount(){
        this.setState({im:this.props.img})
    }

    render(){
        const {im} = this.state
        const {img} = this.props
        return(
            //  <header class="masthead text-white text-center" style={{backgroundColor:'#be88e0'}}>
            <header  style={{backgroundColor:'transparent'}}>
                    {/* <div class="container d-flex align-items-center flex-column">
                        <img class="masthead-avatar mb-5" src={process.env.PUBLIC_URL+"assets/img/avataaars.svg"} alt="" />
                        <h1 class="masthead-heading text-uppercase mb-0">Start Bootstrap</h1>
                        <div class="divider-custom divider-light">
                            <div class="divider-custom-line"></div>
                            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                            <div class="divider-custom-line"></div>
                        </div>
                        <p class="masthead-subheading font-weight-light mb-0">Graphic Artist - Web Designer - Illustrator</p>
                    </div> */}
                    <div name="container">
                    <RBCarousel
                        animation={true}
                        autoplay={true}
                        slideshowSpeed={4500}
                        defaultActiveIndex={0}
                        // leftIcon={this.state.leftIcon}
                        // rightIcon={this.state.rightIcon}
                        //onSelect={this._onSelect}
                        ref={this.slider}
                        version={4}
                        >
                            {/* <div style={{ height: '100%' }}>
                               <img
                                    style={{ width: "100%", height: "100%" }}
                                    src={process.env.PUBLIC_URL+"assets/img/blueberry.jpg"}
                                    />
                                <div className="carousel-caption">blueberry</div>
                           </div>
                           <div style={{ height: '100%' }}>
                               <img
                                    style={{ width: "100%", height: "100%" }}
                                    src={process.env.PUBLIC_URL+"assets/img/orange.jpg"}
                                    />
                                <div className="carousel-caption">orange</div>
                           </div>
                           <div style={{ height: '100%' }}>
                               <img
                                    style={{ width: "100%", height: "100%" }}
                                    src={process.env.PUBLIC_URL+"assets/img/watermelons.jpg"}
                                    />
                                <div className="carousel-caption">watermelons</div>
                           </div> */}
                       {this.props.img.map((data)=>{
                           return(
                                <div style={{ height: '100%' }}>
                                    <img
                                            style={{ width: '100%', height: 600}}
                                            src={data.gbr}
                                            />
                                        <div className="carousel-caption">{data.name}</div>
                                </div>
                           )
                       })}     
                        
                    </RBCarousel>
                    </div>
                </header>
        )
    }
}

export default Header;