import React, { useState } from 'react'
import loadable from '@loadable/component'
import {Fuck} from '../allseeing';
import {Link} from 'react-router-dom'
import { MDBDataTableV5, MDBBtn } from 'mdbreact';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from 'jquery';
import axios from 'axios'
let moment = require('moment')

const Atas = loadable(()=> import('../header/atas')) 
const Header = loadable(()=> import('../header/header'))
const Nav_Detail = loadable(()=> import('../nav/nav_detail'))
const Footer = loadable(()=> import('../footer/footer'))
const Modals = loadable(()=> import('../footer/modals'))
const Bawah = loadable(()=> import('../footer/bawah'))
const Copyright = loadable(()=> import('../footer/copyright'))
const data = [
    {
      name: 'Tiger Nixon',
      position: 'System Architect',
      office: 'Edinburgh',
      age: '61',
      date: '2011/04/25',
      salary: '$320',

    },
    {
      name: 'Garrett Winters',
      position: 'Accountant',
      office: 'Tokyo',
      age: '63',
      date: '2011/07/25',
      salary: '$170',

    },
    {
      name: 'Donna Snider',
      position: 'Customer Support',
      office: 'New York',
      age: '27',
      date: '2011/01/25',
      salary: '$112',

    },
]

    


class Sub extends React.PureComponent{
    constructor(){
        super();
        this.state = {
            img:[],
            menu:[],
            datatable:[],
            show_visit:0
        }
    }

    componentDidMount(){
        // $(document).ready(function () {
        //     $('#example').DataTable();
        // });
        console.log(this.props.location.state.id_kategori)
        this.getSubKategori(this.props.location.state.id_kategori)  
        document.addEventListener('contextmenu', (e) => {
            e.preventDefault();
          });
        this.who_visit()  

    }

    getSubKategori=(id_kategori)=>{
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.subkategori
        console.log(url)
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
              },
            params:{
                id_kategori:id_kategori
            }  
        }).then((response)=>{
                 if(response.data!=0){
                     this.setState({datatable:response.data})
                     $(document).ready(function () {
                        $('#example').DataTable({ordering:false});
                    });
                 }
                 console.log(response.data)
             })
    }

    who_visit=()=>{
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.end_visit
        let who = "random_visitor"
        axios.post(url,{
            kode_kategori:this.props.location.state.id_kategori,
            who:who
        }).then((response)=>{
            if(response.data.sukaes){
                this.show_who_visit()
            }else{
                //this.setState({show:false})
            }
        })
    }
    show_who_visit=()=>{
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.end_show_visit
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
              },
            params:{
                kode_kategori:this.props.location.state.id_kategori,
                tahun:moment().format('YYYY'),
                bulan:moment().format('MM')
            }
        }).then((response)=>{
                //console.log(response.data[0])
                if(response.data != 0){
                    this.setState({show_visit:response.data[0].jumlah_bln_ini})
                    console.log(response.data)
                }else{
                    this.setState({show_visit:0})
                }
        })
    } 


    render(){
        const {datatable,show_visit} = this.state
        const {id_kategori} = this.props.location.state
        
        return(
            <html lang="en">
                <Atas />
                <body id="page-top">
                    <Nav_Detail />
                    {/* <Header img={asu} /> */}
                    <section class="page-section portfolio" id="kategori" style={{backgroundColor:'	#fff'}}>
                        <div class="container" >
                            {/* <div class="row justify-content-end mb-2" >
                                <i class="fa fa-eye" style={{marginRight:5, fontSize:14,marginTop:3}} />
                                <text style={{fontSize:14}}>{show_visit}</text>
                            </div> */}
                            <table id="example" class="table table-hover" style={{marginTop:15}}>
                                <thead>
                                    <tr>
                                    <th>kode</th>
                                    <th>sub kategori</th>
                                    <th>aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {datatable.map((result) => {
                                            return (
                                            
                                                <tr>
                                                <td>{result.kode}</td>
                                                <td>{result.sub_kategori}</td>
                                                <td>
                                                    <Link to={{pathname:'/End' , state:{kode:result.kode, sub_kategori:result.sub_kategori, tbl:result.tbl, nb:result.keterangan}}}>
                                                            <text>lihat</text>
                                                    </Link>
                                                </td>    
                                                </tr>
                                            
                                            )
                                        })
                                    } 
                                
                                </tbody>
                            </table>
                        </div>
                    </section>
                    {/* <section class="page-section text-white mb-0" id="download" style={{backgroundColor:'#fff'}}>
                        <div class="container">
                                <h2 class="page-section-heading text-center text-uppercase" style={{color:'#be88e0'}}>Download</h2>
                                <h3 class="text-center text-uppercase" style={{color:'#be88e0'}}>Masih Kosong Lur :D</h3>
                        </div>        
                    </section>
                    <section class="page-section text-white mb-0" id="about" style={{backgroundColor:'#be88e0'}}>
                        <div class="container">
                            <h2 class="page-section-heading text-center text-uppercase text-white">About</h2>
                            <div class="divider-custom divider-light">
                                <div class="divider-custom-line"></div>
                                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                <div class="divider-custom-line"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 ml-auto"><p class="lead">Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional SASS stylesheets for easy customization.</p></div>
                                <div class="col-lg-4 mr-auto"><p class="lead">You can create your own custom avatar for the masthead, change the icon in the dividers, and add your email address to the contact form to make it fully functional!</p></div>
                            </div>
                            <div class="text-center mt-4">
                                <a class="btn btn-xl btn-outline-light" href="https://startbootstrap.com/theme/freelancer/">
                                    <i class="fas fa-download mr-2"></i>
                                    Free Download!
                                </a>
                            </div>
                        </div>
                    </section> */}
                    {/* <section class="page-section" id="contact">
                        <div class="container">
                            <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Me</h2>
                            <div class="divider-custom">
                                <div class="divider-custom-line"></div>
                                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                <div class="divider-custom-line"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 mx-auto">
                                    <form id="contactForm" name="sentMessage" novalidate="novalidate">
                                        <div class="control-group">
                                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                                <label>Name</label>
                                                <input class="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name." />
                                                <p class="help-block text-danger"></p>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                                <label>Email Address</label>
                                                <input class="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address." />
                                                <p class="help-block text-danger"></p>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                                <label>Phone Number</label>
                                                <input class="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number." />
                                                <p class="help-block text-danger"></p>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                                <label>Message</label>
                                                <textarea class="form-control" id="message" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."></textarea>
                                                <p class="help-block text-danger"></p>
                                            </div>
                                        </div>
                                        <br />
                                        <div id="success"></div>
                                        <div class="form-group"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Send</button></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section> */}
                    
                    {/* <Footer /> */}
                    {/* <Copyright /> */}
                    {/* <Modals /> */}
                    <Bawah />
                </body>
            </html>
        )
    }
}
let buS = Fuck(Sub)
export default buS;