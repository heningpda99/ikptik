import React, { useState } from 'react'
import loadable from '@loadable/component'
import {Fuck} from '../allseeing';
import {add_to_datatable,clear_datatable,add_to_datatable_filter,clear_datatable_filter} from '../rdx/action'
import { MDBDataTableV5, MDBBtn } from 'mdbreact';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $, { data } from 'jquery';
import axios from 'axios'
import {Bar} from 'react-chartjs-2'
import {ExportToCsv} from 'export-to-csv'
import {Modal,Button,Form,InputGroup} from 'react-bootstrap'
import {Emoji} from 'react-emoji-render'
let moment = require('moment')
var _ = require('lodash');

const Atas = loadable(()=> import('../header/atas')) 
const Header = loadable(()=> import('../header/header'))
const Nav_Detail = loadable(()=> import('../nav/nav_detail'))
const Footer = loadable(()=> import('../footer/footer'))
const Modals = loadable(()=> import('../footer/modals'))
const Bawah = loadable(()=> import('../footer/bawah'))
const Copyright = loadable(()=> import('../footer/copyright'))
const Nav_End = loadable(()=> import('../nav/nav_end'))
const Visua = loadable(()=> import ('../visua/visua'))
    

class End extends React.PureComponent{
    constructor(){
        super();
        this.state = {
            img:[],
            menu:[],
            datatable:[],
            kolom:[],
            show:false,
            who:'',
            show_download:[],
            tahun:[],
            
        }

    }

    componentDidMount(){
        const kode = this.props.location.state.kode
        const tbl = this.props.location.state.tbl
         console.log(kode)
         console.log(tbl)
        this.show_who_download()
        // $(document).ready(function () {
        //     $('#example1').DataTable();
        // });
        if(kode == "1.1.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "1.1.2" ){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "1.1.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "1.1.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "1.2.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "1.2.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "1.2.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}  
        else if(kode == "1.3.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "1.3.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "1.3.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "1.4.1"){this.get_1_4_1(kode,tbl)}
        else if(kode == "2.1.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "2.1.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "2.1.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "2.1.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "2.2.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "2.2.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "2.2.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "2.3.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "2.3.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "2.3.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.1.1"){this.get_one_for_all(kode,tbl)}   
        else if(kode == "3.2.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "3.2.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.2.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.2.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "3.3.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.3.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.3.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.4.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "3.5.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.5.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.5.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.6.1"){this.get_Satu_Pintu_Bulan(kode,tbl)}   
        else if(kode == "3.7.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "3.7.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.7.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "3.7.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.1.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "4.1.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.1.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.1.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.1.5"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.1.6"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.1.7"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.2.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "4.2.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.2.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.2.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.2.5"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.2.6"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.2.7"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.3.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "4.3.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.3.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.3.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.4.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "4.4.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.4.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.4.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.5.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.5.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.5.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.6.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.6.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.6.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "4.7.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.7.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.7.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "4.8.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.8.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.8.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "4.9.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.9.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.9.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "4.10.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.10.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "4.10.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "4.11.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "4.11.2"){this.get_one_for_all(kode,tbl)}   
        else if(kode == "4.11.3"){this.get_one_for_all(kode,tbl)}   
        else if(kode == "4.11.4"){this.get_one_for_all(kode,tbl)}   
        else if(kode == "4.11.5"){this.get_one_for_all(kode,tbl)}   
        else if(kode == "5.1.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "5.1.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.1.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.1.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.2.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "5.2.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.2.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.2.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.3.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "5.3.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.3.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.3.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.4.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.4.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.4.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.5.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.5.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.5.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "5.6.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "5.6.2"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "5.7.1"){this.get_Satu_Pintu_Bulan(kode,tbl)}   
        else if(kode == "5.7.2"){this.get_Satu_Pintu_Bulan(kode,tbl)}   
        else if(kode == "5.7.3"){this.get_Satu_Pintu_Bulan(kode,tbl)}   
        else if(kode == "5.8.1"){this.get_Satu_Pintu_Bulan(kode,tbl)}   
        else if(kode == "5.8.2"){this.get_Satu_Pintu_Bulan(kode,tbl)}
        else if(kode == "5.9.1"){this.get_one_for_all(kode,tbl)}   
        else if(kode == "5.9.2"){this.get_one_for_all(kode,tbl)}   
        else if(kode == "5.9.3"){this.get_one_for_all(kode,tbl)}   
        else if(kode == "6.1.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "6.1.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "6.1.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "6.1.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "6.2.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "6.2.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "6.2.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "6.2.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "6.3.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "6.3.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "6.3.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "6.4.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "6.4.2"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "6.4.3"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "6.4.4"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "6.5.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "7.1.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "7.1.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.1.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.1.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.2.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.2.2"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.2.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.2.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.2.5"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.3.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.3.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.3.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.4.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.4.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.4.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.4.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.5.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.5.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.5.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.5.4"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "7.6.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.6.2"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.7.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.7.2"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.8.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.9.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.9.2"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.10.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.10.2"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.11.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "7.12.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "7.12.2"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "8.1.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "8.1.2"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "8.1.3"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "8.2.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "8.3.1"){this.get_Satu_Pintu_Kecamatan(kode,tbl)}
        else if(kode == "8.4.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "8.4.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}   
        else if(kode == "8.4.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "9.1.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.1.2"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.2.1"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}        
        else if(kode == "9.2.2"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "9.2.3"){this.get_Satu_Pintu_Kelurahan(kode,tbl)}
        else if(kode == "9.3.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.3.2"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.4.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.4.2"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.5.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.5.2"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.6.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.6.2"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.6.3"){this.get_Satu_Pintu_Daerah(kode,tbl)}
        else if(kode == "9.7.1"){this.get_Satu_Pintu_Daerah(kode,tbl)}
        else if(kode == "9.7.2"){this.get_Satu_Pintu_Daerah(kode,tbl)}
        else if(kode == "9.8.1"){this.get_Satu_Pintu_Daerah(kode,tbl)}
        else if(kode == "9.9.1"){this.get_Satu_Pintu_Daerah(kode,tbl)}
        else if(kode == "9.10.1"){this.get_Satu_Pintu_Daerah(kode,tbl)}
        else if(kode == "9.10.2"){this.get_Satu_Pintu_Daerah(kode,tbl)}
        else if(kode == "9.11.1"){this.get_Satu_Pintu_Daerah(kode,tbl)}
        else if(kode == "9.11.2"){this.get_Satu_Pintu_Daerah(kode,tbl)}
        else if(kode == "9.12.1"){this.get_one_for_all(kode,tbl)}
        else if(kode == "9.12.2"){this.get_Satu_Pintu_Daerah(kode,tbl)}



    }

    get_Satu_Pintu_Kecamatan=(kode,tbl)=>{
        this.props.clear_datatable()
        this.props.clear_datatable_filter()

        let col = []
        let row = []
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.satu_pintu_kecamatan
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
              },
            params:{
                table:tbl,
                table:tbl,
                table:tbl,
                kode_sub_kategori:kode
            }  
        }).then((response)=>{
                 if(response.data!=0){
                     this.setState({datatable:response.data})
                     this.props.add_to_datatable(response.data)
                     this.props.add_to_datatable_filter(response.data)
                     let kirik = response.data
                     let asu = Object.keys(response.data[0])
                    //  const requiredObject = asu.reduce((obj, val) => {
                    //     obj[val] = val;
                    //     return obj;
                    //   }, {});
         
                    asu.map((reo)=>{
                        //col.push(requiredObject)
                        //console.log(reo)
                            col.push({'title':reo})
                        
                    })
                    let yeoh = kirik.map(Object.values)
                    
                    //console.log(JSON.stringify(col))
                    console.log(response.data)
                    this.setState({kolom:col})

                     $(document).ready(function () {
                        let tbl = $('#example1').DataTable({
                            ordering:false,
                            columns:col,
                            data:yeoh
                        });
                        
                    });
                 }
                 //console.log(response.data)
             })
    }

    get_Satu_Pintu_Kelurahan=(kode,tbl)=>{
        this.props.clear_datatable()
        this.props.clear_datatable_filter()
        let col = []
        let row = []
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.satu_pintu_kelurahan
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
              },
            params:{
                table:tbl,
                table:tbl,
                table:tbl,
                kode_sub_kategori:kode
            }  
        }).then((response)=>{
                 if(response.data!=0){
                     this.setState({datatable:response.data})
                     this.props.add_to_datatable(response.data)
                     this.props.add_to_datatable_filter(response.data)
                     let kirik = response.data
                     let asu = Object.keys(response.data[0])

                    asu.map((reo)=>{
                            col.push({'title':reo})
                        
                    })
                    let yeoh = kirik.map(Object.values)
                    
                    this.setState({kolom:col})

                     $(document).ready(function () {
                        let tbl = $('#example1').DataTable({
                            ordering:false,
                            columns:col,
                            data:yeoh
                        });
                        
                    });
                 }
               
             })
    }

    get_Satu_Pintu_Bulan=(kode,tbl)=>{
        this.props.clear_datatable()
        this.props.clear_datatable_filter()
        let col = []
        let row = []
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.satu_pintu_bulan
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
              },
            params:{
                table:tbl,
                table:tbl,
                table:tbl,
                kode_sub_kategori:kode
            }  
        }).then((response)=>{
                 if(response.data!=0){
                     this.setState({datatable:response.data})
                     this.props.add_to_datatable(response.data)
                     this.props.add_to_datatable_filter(response.data)
                     let kirik = response.data
                     let asu = Object.keys(response.data[0])

                    asu.map((reo)=>{
                            col.push({'title':reo})
                        
                    })
                    let yeoh = kirik.map(Object.values)
                    
                    this.setState({kolom:col})

                     $(document).ready(function () {
                        let tbl = $('#example1').DataTable({
                            ordering:false,
                            columns:col,
                            data:yeoh
                        });
                        
                    });
                 }
               
             })
    }
    get_Satu_Pintu_Daerah=(kode,tbl)=>{
        this.props.clear_datatable()
        this.props.clear_datatable_filter()
        let col = []
        let row = []
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.satu_pintu_daerah
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
              },
            params:{
                table:tbl,
                table:tbl,
                table:tbl,
                kode_sub_kategori:kode
            }  
        }).then((response)=>{
                 if(response.data!=0){
                     this.setState({datatable:response.data})
                     this.props.add_to_datatable(response.data)
                     this.props.add_to_datatable_filter(response.data)
                     let kirik = response.data
                     let asu = Object.keys(response.data[0])

                    asu.map((reo)=>{
                            col.push({'title':reo})
                        
                    })
                    let yeoh = kirik.map(Object.values)
                    
                    this.setState({kolom:col})

                     $(document).ready(function () {
                        let tbl = $('#example1').DataTable({
                            ordering:false,
                            columns:col,
                            data:yeoh
                        });
                        
                    });
                 }
               
             })
    }

    get_1_4_1=(kode,tbl)=>{
        this.props.clear_datatable()
        this.props.clear_datatable_filter()
        let col = []
        let row = []
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.end_1_4_1
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
              },
            params:{
                kode_sub_kategori:kode
            }  
        }).then((response)=>{
                 if(response.data!=0){
                     this.setState({datatable:response.data})
                     this.props.add_to_datatable(response.data)
                     this.props.add_to_datatable_filter(response.data)
                     let kirik = response.data
                     let asu = Object.keys(response.data[0])

                    asu.map((reo)=>{
                            col.push({'title':reo})
                        
                    })
                    let yeoh = kirik.map(Object.values)
                    
                    this.setState({kolom:col})

                     $(document).ready(function () {
                        let tbl = $('#example1').DataTable({
                            ordering:false,
                            columns:col,
                            data:yeoh
                        });
                        
                    });
                 }
               
             })
    }
    get_one_for_all=(kode,tbl)=>{
        this.props.clear_datatable()
        this.props.clear_datatable_filter()
        let col = []
        let row = []
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.one_for_all
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
              },
            params:{
                table:tbl,
                kode_sub_kategori:kode
            }  
        }).then((response)=>{
                 if(response.data!=0){
                     this.setState({datatable:response.data})
                     this.props.add_to_datatable(response.data)
                     this.props.add_to_datatable_filter(response.data)

                     let kirik = response.data
                     let asu = Object.keys(response.data[0])

                    asu.map((reo)=>{
                            col.push({'title':reo})
                        
                    })
                    let yeoh = kirik.map(Object.values)
                    
                    this.setState({kolom:col})

                     $(document).ready(function () {
                        let tbl = $('#example1').DataTable({
                            ordering:false,
                            columns:col,
                            data:yeoh
                        });
                        
                    });
                 }
               
             })
    }

    XPORT=()=>{
        const {datatable} = this.state;
        const options = { 
            filename: this.props.location.state.sub_kategori,
            fieldSeparator: ';',
            quoteStrings: '"',
            decimalSeparator: '.',
            showLabels: true, 
            showTitle: true,
            title: this.props.location.state.sub_kategori,
            useTextFile: false,
            useBom: true,
            useKeysAsHeaders: true,
            // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
          };
          const csvExporter = new ExportToCsv(options);
          csvExporter.generateCsv(datatable);

    }


    who_post=()=>{
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.end_download
        if(this.state.who == ""){
            this.setState({who:"-"})
        }
        axios.post(url,{
            kode_sub_kategori:this.props.location.state.kode,
            who:this.state.who
        }).then((response)=>{
            if(response.data.sukaes){
                this.setState({show:false})
                this.XPORT()
                this.show_who_download()
            }else{
                //this.setState({show:false})
            }
        })
    }
    show_who_download=()=>{
        let url = this.props.reds.url.UerEls+this.props.reds.url.routes.end_show_download
        axios.get(url,{
            headers: {
                'Access-Control-Allow-Origin': true,
              },
            params:{
                kode_sub_kategori:this.props.location.state.kode,
                tahun:moment().format('YYYY'),
                bulan:moment().format('MM')
            }
        }).then((response)=>{
                //console.log(response.data[0])
                if(response.data != 0){
                    this.setState({show_download:response.data[0].jumlah_bln_ini})
                }else{
                    this.setState({show_download:0})
                }
        })
    }  

    render(){
        const {datatable,kolom,show_download} = this.state;
        const {nb} = this.props.location.state
        const data_in_redux = this.props.prod.datatable
        //console.log(datatable)
        return(
            <html lang="en">
                <Atas />
                <body id="page-top">
                    <Nav_End sub_kategori={this.props.location.state.sub_kategori}/>
                    {/* <Header img={asu} /> */}
                    <section class="page-section portfolio" id="kategori" style={{backgroundColor:'	#fff'}}>
                    {(datatable.length !=0)?
                        <div class="container" >
                            
                            <table id="example1" class="table table-hover" >
                                
                            </table>
                                <div class="row justify-content-end" style={{marginTop:5}}>
                                    {/* <i class="fa fa-download" style={{marginRight:5,fontSize:20,color:'purple'}}/>
                                    <text style={{marginRight:20}}>{show_download}x</text> */}
                                    
                                    <div role="button" onClick={()=>this.setState({show:true})}>
                                        {/* <i class="fa fa-download" style={{color:'purple',fontSize:20,marginRight:2.5}}/> */}
                                        <text>download csv</text>
                                    </div>
                                </div> 
                                <div class="row justify-content-start" style={{marginTop:5}}>
                                    {/* <i class="fa fa-download" style={{marginRight:5,fontSize:20,color:'purple'}}/>
                                    <text style={{marginRight:20}}>{show_download}x</text> */}
                                    
                                    <div>
                                        {/* <i class="fa fa-download" style={{color:'purple',fontSize:20,marginRight:2.5}}/> */}
                                        <text style={{color:'grey',fontsize:10}}>{nb}</text>
                                    </div>
                                </div>   
                        </div>
                         :
                         <div class="row justify-content-center">
                             <img src={process.env.PUBLIC_URL+"assets/img/portfolio/data_not_found.png"}/>
                         </div>
                    }

                            <Modal show={this.state.show} onHide={()=>this.setState({show:false})} >
                                <Modal.Header closeButton={true} style={{backgroundColor:'#000',color:'#fff'}}>
                                    {/* <Modal.Title >Ini Siapa yang Download ?</Modal.Title> */}
                                    <Modal.Title >Siapa nih ?</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <InputGroup>
                                    <Form.Control type="email" placeholder="Saya Adalah ....." onChange = {(event) => this.setState({who:event.target.value})}/>
                                    <InputGroup.Append>
                                        <Button variant="outline-secondary" onClick={()=>this.who_post()}>Lanjut Download</Button>
                                    </InputGroup.Append>
                                    </InputGroup>
                                </Modal.Body>
                            </Modal> 
                    </section>
                    <section class="page-section text-white mb-0" id="download" style={{backgroundColor:'#000'}}>
                        <div class="container" >
                            {(data_in_redux.length != 0)?
                                <Visua datatable={datatable} kolom={kolom} kode={this.props.location.state.kode} sub_kategori={this.props.location.state.sub_kategori}/>               
                                :null
                            }
                        </div>
                    </section>

                    <Modals />
                    <Bawah />
                </body>
            </html>
        )
    }
}
let dnE = Fuck(End)
export default dnE;